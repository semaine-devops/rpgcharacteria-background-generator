import generatePrompt from "../utils/prompt";

test('test', () => {
    const prompt = generatePrompt('anis','25','male','warrior','human', 'burned')
    expect(prompt).toBe(`You are a role-playing master, and you want to create a story around a character, this character is called Anis, he is a male human warrior, is 25 years old, and is burned. Write his story in 50 words.`)
});
