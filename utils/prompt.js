function generatePrompt(name, age, gender, characterClass, race, physique) {
    const capitalizedName = name[0].toUpperCase() + name.slice(1).toLowerCase();
    console.log(capitalizedName)
    return `You are a role-playing master, and you want to create a story around a character, this character is called ${capitalizedName}, ${gender === 'Female'? 'she' : 'he'} is a ${gender} ${race} ${characterClass}, is ${age} years old, and is ${physique}. Write his story in 50 words.`;
}
module.exports = generatePrompt