var express = require('express');
var router = express.Router();
const jwt = require('jsonwebtoken');
require('dotenv').config()
var openAi = require('openai')
const generatePrompt = require("../utils/prompt");
const configuration = new openAi.Configuration({
    apiKey: process.env.OPENAI_API_KEY,
});
const openai = new openAi.OpenAIApi(configuration);

/* GET home page. */
router.post('/generate', async function (req, res, next) {
console.log('background generator')
    const jwtToken = req.body.jwtToken
    console.log(jwtToken)
    const verify = jwt.verify(jwtToken, process.env.SECRET)
    if (!verify) {
        console.log('not verify')
        res.status(403).json({
            error: {
                message: "Unauthorized",
            }
        });
        return;
    }
    if (!configuration.apiKey) {
        console.log('no api key')
        res.status(500).json({
            error: {
                message: "OpenAI API key not configured, please follow instructions in README.md",
            }
        });
        return;
    }
    const prompt = generatePrompt(req.body.name, req.body.age, req.body.gender, req.body.characterClass, req.body.race, req.body.physique);
    const completion = await openai.createCompletion({
        model: "text-davinci-003",
        prompt: prompt,
        max_tokens: 200,
    });
    console.log(prompt)
    res.status(200).json({result: completion.data.choices[0].text});


});

module.exports = router;
